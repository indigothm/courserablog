class Post < ActiveRecord::Base
has_many :comments, dependent: :destroy  
validates_presense_of :title
validates_presense_of :body
end
